// Instructions for compilation & flashing:
// Run "sudo make hotdox:mwilliams1124:flash" in the QMK root directory
// Then press boot mode button on keyboard
//
// Use this function from Cam's source instead of clear_mods()
// - if using clear_mods() the mods will remain cleared after
// the keypress is complete even if the mod is still held
//
/* #define WITHOUT_MODS(...) \
     do { \
       uint8_t _real_mods = get_mods(); \
       clear_mods(); \
       { __VA_ARGS__ } \
       set_mods(_real_mods); \
     } while (0) 				*/

#include QMK_KEYBOARD_H
#include "version.h"
#include "quantum.h"

// Layer declarations
#define PRIMARY 0 // Default layer
#define NUMPAD 1 // WIP
#define VIM_KEYS 2 // WIP
#define FUNCTION 3 // WIP
#define DOUBLES 4 // WIP
//#define BLANK 20 // Blank, unused layer.
// Note; if the last layer number is too high, the firmware gets too large

enum custom_keycodes {
  RGB_SLD = SAFE_RANGE, // can always be here
  TOGGLE_LAYER_COLOR,
  EPRM,
  DYNAMIC_MACRO_RANGE,
  QMKBEST,
  AUTO_PAREN,
  AUTO_BRACKET,
  AUTO_QUOTE,
};

// Tap Dance keycodes
enum {
  OMNIBRKT_L = 0,
  OMNIBRKT_R,
  MNUS_EQ,
//  ELSFT,
  DBLSHFT_L,
  DBLSHFT_R,
  VIM_ESC,
};

// Tap Dance declarations

//Function associated with all tap dances
int cur_dance (tap_dance_state_t *state);

//Functions associated with individual tap dances
void doubles_action_left (tap_dance_state_t *state, void *user_data);
void doubles_reset_left (tap_dance_state_t *state, void *user_data);
void doubles_action_right (tap_dance_state_t *state, void *user_data);
void doubles_reset_right (tap_dance_state_t *state, void *user_data);
void vim_keys_esc (tap_dance_state_t *state, void *user_data);
void vim_keys_esc_reset (tap_dance_state_t *state, void *user_data);

// ::::: KEYMAPS {{{1

// Considerations/thoughts:
// Shift, Ctrl & Alt output their normal value and at same time switch to function layer.
// Need another layer key for function layer without chording
// LM(layer, mod) - Momentarily activates layer (like MO), but with modifier(s) mod active. Only supports layers 0-15 and the left modifiers: MOD_LCTL, MOD_LSFT, MOD_LALT, MOD_LGUI (note the use of MOD_ constants instead of KC_). These modifiers can be combined using bitwise OR, e.g. LM(FUNC, MOD_LCTL | MOD_LALT).
//
// Pad layer - shebangs, boilerplate etc
// Use Tap Dance for RESET (bootmagic keycode)?

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

[PRIMARY] = LAYOUT_ergodox( //:{{{2

    /* keymap-diagram:
     * ,--------------------------------------------------.    ,--------------------------------------------------.
     * |    ~   |   1  |   2  |   3  |   4  |   5  | '""' |    | '""' |   6  |   7  |   8  |   9  |   0  |  -/=   |
     * |--------+------+------+------+------+------+------|    |------+------+------+------+------+------+--------|
     * |  Tab   |   Q  |   W  |   E  |   R  |   T  |  ()  |    |  []  |   Y  |   U  |   I  |   O  |   P  |   \    |
     * |--------+------+------+------+======+------|      |    |      |------+======+------+------+------+--------|
     * |  Ctrl  |   A  |   S  |   D  |   F  |   G  |------|    |------|   H  |   J  |   K  |   L  |   ;  |   '    |
     * |--------+------+------+------+======+------|  [{( |    | )}]  |------+======+------+------+------+--------|
     * | DbShft |   Z  |   X  |   C  |   V  |   B  |      |    |      |   N  |   M  |   ,  |   .  |   /  | DbShft |
     * `--------+------+------+------+------+-------------'    `-------------+------+------+------+------+--------'
     *   | PgUp | PgDn | Home | End  |VimEsc|                                | Del  | Ins  | Meh  | SysR | Hypr |
     *   `----------------------------------'                                `----------------------------------'
     *                                       ,-------------.  ,-------------.
     *                                       | Meta |  Fx  |  |  Fx  | Meta |
     *                                ,------+------+------|  |------+------+------.
     *                                |      |      | NmPd |  |      |      |      |
     *                                | Entr | Bksp |------|  |------| Ctrl | Spc  |
     *                                |      |      | Alt  |  | Alt  |      |      |
     *                                `--------------------'  `--------------------'
     */
//Caps KC_CAPSLOCK - make this available somewhere. Shift double tap??
//Remove one of the Fx layer keys once you have auto-switch-to-fx-layer-with-modifier working

// Left Hand {{{3

KC_GRAVE              , KC_1     , KC_2   , KC_3  , KC_4       , KC_5   , AUTO_QUOTE    ,
KC_TAB                , KC_Q     , KC_W   , KC_E  , KC_R       , KC_T   , AUTO_PAREN    ,
LM(FUNCTION, MOD_LCTL), KC_A     , KC_S   , KC_D  , KC_F       , KC_G   ,
TD(DBLSHFT_L)         , KC_Z     , KC_X   , KC_C  , KC_V       , KC_B   , TD(OMNIBRKT_L),
KC_PGUP               , KC_PAGE_DOWN, KC_HOME, KC_END, TD(VIM_ESC),						            // 3}}}

// Left Thumb {{{3

          KC_LGUI      , MO(3)      ,
                         MO(NUMPAD) ,
KC_ENTER, KC_BACKSPACE    , LM(FUNCTION, MOD_LALT),													// 3}}}

// Right Hand {{{3

AUTO_QUOTE    , KC_6, KC_7     , KC_8     , KC_9  , KC_0      , TD(MNUS_EQ)  ,
AUTO_BRACKET  , KC_Y, KC_U     , KC_I     , KC_O  , KC_P      , KC_BACKSLASH    ,
                KC_H, KC_J     , KC_K     , KC_L  , KC_SEMICOLON , KC_QUOTE     ,
TD(OMNIBRKT_R), KC_N, KC_M     , KC_COMMA , KC_DOT, KC_SLASH  , TD(DBLSHFT_R),
                      KC_DELETE, KC_INSERT, KC_MEH, KC_PRINT_SCREEN, KC_HYPR      ,						// 3}}}

// Right Thumb {{{3

MO(3)      , KC_RGUI  ,
_______    ,
LM(FUNCTION, MOD_LALT), LM(FUNCTION, MOD_LCTL), KC_SPACE											//3}}}

), // 2}}}

[NUMPAD] = LAYOUT_ergodox( //:{{{2

    /* keymap-diagram:
     * ,--------------------------------------------------.    ,--------------------------------------------------.
     * |        |      |      |      |      |      |      |    | vol x|      |  =   |  \   |  *   |   -  |        |
     * |--------+------+------+------+------+------+------|    |------+------+------+------+------+------+--------|
     * |        |      |      |      |      |      |      |    | vol+ |      |   7  |   8  |   9  |   +  |        |
     * |--------+------+------+------+======+------|      |    |      |------+======+------+------+------+--------|
     * |        |      |      |      |      |      |------|    |------|      |   4  |   5  |   6  |   +  |        |
     * |--------+------+------+------+======+------|      |    | vol- |------+======+------+------+------+--------|
     * |        |      |      |      |      |      |      |    |      |      |   1  |   2  |   3  | Entr |        |
     * `--------+------+------+------+------+-------------'    `-------------+------+------+------+------+--------'
     *   |      |      |      |      |      |                                |   0  |   0  |   .  | Entr |      |
     *   `----------------------------------'                                `----------------------------------'
     *                                       ,-------------.  ,-------------.
     *                                       |      |      |  | Rew  |  FF  |
     *                                ,------+------+------|  |------+------+------.
     *                                |      |      |      |  | Prev |      |      |
     *                                |      |      |------|  |------|      |      |
     *                                |      |      |      |  | Next |      |      |
     *                                `--------------------'  `--------------------'
     */

// Left Hand {{{3

_______  , _______, _______, _______, _______, _______, _______          ,
_______  , _______, _______, _______, _______, _______, KC_AUDIO_VOL_DOWN,
_______  , _______, _______, _______, _______, _______,
_______  , _______, _______, _______, _______, _______, _______          ,
_______  , _______, _______, _______, _______,														//3}}}

// Left Thumb {{{3

         _______, _______,
                  _______,
_______, _______, _______,																			//3}}}

// Right Hand {{{3


KC_AUDIO_MUTE    , _______, KC_KP_EQUAL, KC_KP_SLASH, KC_KP_ASTERISK, KC_KP_MINUS, _______,
KC_AUDIO_VOL_UP  , _______, KC_KP_7    , KC_KP_8    , KC_KP_9       , KC_KP_PLUS , _______,
                   _______, KC_KP_4    , KC_KP_5    , KC_KP_6       , KC_KP_PLUS , _______,
KC_AUDIO_VOL_DOWN, _______, KC_KP_1    , KC_KP_2    , KC_KP_3       , KC_KP_ENTER, _______,
                            KC_KP_0    , KC_KP_0    , KC_KP_DOT     , KC_KP_ENTER, _______,			//3}}}

// Right Thumb {{{3

KC_MEDIA_REWIND    , KC_MEDIA_FAST_FORWARD,
KC_MEDIA_PREV_TRACK,
KC_MEDIA_NEXT_TRACK, _______              , _______													//3}}}

), //2}}}

[VIM_KEYS] = LAYOUT_ergodox( //:{{{2

    /* keymap-diagram:
     * ,--------------------------------------------------.    ,--------------------------------------------------.
     * |        |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |    |  F7  |  F8  |  F9  |  F10 |  F11 | F12  |        |
     * |--------+------+------+------+------+------+------|    |------+------+------+------+------+------+--------|
     * |        |      |      |  Up  |      |      |      |    |      |      |      |      |      |      |        |
     * |--------+------+------+------+======+------|      |    |      |------+======+------+------+------+--------|
     * |        |      | Left | Down | Right|      |------|    |------| Left | Down |  Up  | Right|      |        |
     * |--------+------+------+------+======+------|      |    |      |------+======+------+------+------+--------|
     * |        |      | Ms2  | Ms3  | Ms1  |      |      |    |      |      |      |      |      |      |        |
     * `--------+------+------+------+------+-------------'    `-------------+------+------+------+------+--------'
     *   |      |      |      |      |      |                                |      |      |      |      |      |
     *   `----------------------------------'                                `----------------------------------'
     *                                       ,-------------.  ,-------------.
     *                                       | WhlUp|WhlDwn|  | WhlL | WhlR |
     *                                ,------+------+------|  |------+------+------.
     *                                |      |      |      |  |      |      |      |
     *                                |      |      |------|  |------|      |      |
     *                                |      |      |      |  |      |      |      |
     *                                `--------------------'  `--------------------'
     */

// Left Hand {{{3

_______, KC_F1  , KC_F2     , KC_F3     , KC_F4      , KC_F5  , KC_F6  ,
_______, _______, _______   , KC_MS_UP  , _______    , _______, _______,
_______, _______, KC_MS_LEFT, KC_MS_DOWN, KC_MS_RIGHT, _______,
_______, _______, KC_MS_BTN2, KC_MS_BTN3, KC_MS_BTN1 , _______, _______,
_______, _______, _______   , _______   , _______    ,												//3}}}

// Left Thumb {{{3

         KC_MS_WH_UP, KC_MS_WH_DOWN,
                      _______      ,
_______, _______    , _______      ,																//3}}}

// Right Hand {{{3

KC_F7  , KC_F8  , KC_F9  , KC_F10 , KC_F11  , KC_F12 , _______,
_______, _______, _______, _______, _______ , _______, _______,
         KC_LEFT, KC_DOWN, KC_UP  , KC_RIGHT, _______, _______,
_______, _______, _______, _______, _______ , _______, _______,
                  _______, _______, _______ , _______, _______,										//3}}}

// Right Thumb {{{3

KC_MS_WH_LEFT, KC_MS_WH_RIGHT,
_______      ,
_______      , _______       , _______																//3}}}

), //2}}}

[FUNCTION] = LAYOUT_ergodox( //:{{{2

    /* keymap-diagram:
     * ,--------------------------------------------------.    ,--------------------------------------------------.
     * |        |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |    |  F7  |  F8  |  F9  | F10  | F11  | F12  |        |
     * |--------+------+------+------+------+------+------|    |------+------+------+------+------+------+--------|
     * |        |      |      |      |      |      |      |    |      |      |      |      |      |      |        |
     * |--------+------+------+------+======+------|      |    |      |------+======+------+------+------+--------|
     * |        |      |      |      |      |      |------|    |------|      |      |      |      |      |        |
     * |--------+------+------+------+======+------| Mcr1 |    | Mcr2 |------+======+------+------+------+--------|
     * |        |      |      |      |      |      | Play |    | Play |      |      |      |      |      |        |
     * `--------+------+------+------+------+-------------'    `-------------+------+------+------+------+--------'
     *   |      |      | F13  | F14  | F15  |                                | F16  | F17  | F18  | F19  | F20  |
     *   `----------------------------------'                                `----------------------------------'
     *                                       ,-------------.  ,-------------.
     *                                       |McrRc1|McrRc2|  |      |      |
     *                                ,------+------+------|  |------+------+------.
     *                                |      |      |      |  |      |      |      |
     *                                |      |      |------|  |------|      |      |
     *                                |      |      |      |  |      |      |      |
     *                                `--------------------'  `--------------------'
     */

// Left Hand {{{3

_______, KC_F1  , KC_F2  , KC_F3  , KC_F4  , KC_F5  , KC_F6          ,
_______, _______, _______, _______, _______, _______, _______        ,
_______, _______, _______, _______, _______, _______,
_______, _______, _______, _______, _______, _______, QK_DYNAMIC_MACRO_PLAY_1,
_______, _______, KC_F13 , KC_F14 , KC_F15 ,														//3}}}

// Left Thumb {{{3

         QK_DYNAMIC_MACRO_RECORD_START_1, QK_DYNAMIC_MACRO_RECORD_START_2,
                         _______       ,
_______, _______       , _______       ,															//3}}}

// Right Hand {{{3

KC_F7          , KC_F8  , KC_F9  , KC_F10 , KC_F11 , KC_F12 , _______,
_______        , _______, _______, _______, _______, _______, _______,
                 _______, _______, _______, _______, _______, _______,
QK_DYNAMIC_MACRO_PLAY_2, _______, _______, _______, _______, _______, _______,
                          KC_F16 , KC_F17 , KC_F18 , KC_F19 , KC_F20 ,								//3}}}

// Right Thumb {{{3

_______, _______,
_______,
_______, _______, _______																			//3}}}

), //2}}}

[DOUBLES] = LAYOUT_ergodox( //:{{{2

    /* keymap-diagram:
     * ,--------------------------------------------------.    ,--------------------------------------------------.
     * |        |      |      |      |      |      |      |    |      |      |      |      |      |      |        |
     * |--------+------+------+------+------+------+------|    |------+------+------+------+------+------+--------|
     * |        |      |      |      |      |      |      |    |      |      |      |      |      |      |   X    |
     * |--------+------+------+------+======+------|      |    |      |------+======+------+------+------+--------|
     * |        |      |      |      |      |      |------|    |------|      |      |      |      |      |        |
     * |--------+------+------+------+======+------| qmk  |    | []{} |------+======+------+------+------+--------|
     * |        |      |      |      |      |      |      |    |  ()  |      |      |      |      |      |        |
     * `--------+------+------+------+------+-------------'    `-------------+------+------+------+------+--------'
     *   |      |      |      |      |      |                                |      |      |      |      |      |
     *   `----------------------------------'                                `----------------------------------'
     *                                       ,-------------.  ,-------------.
     *                                       |      |      |  |      |      |
     *                                ,------+------+------|  |------+------+------.
     *                                |      |      |      |  |      |      |      |
     *                                |      |      |------|  |------|      |      |
     *                                |      |      |      |  |      |      |      |
     *                                `--------------------'  `--------------------'
     */

//double tap shift, it takes you the doubles/=+ layer, tap then hold shift, it takes you to the doubles layer and holds shift

// Left Hand {{{3

_______, _______, _______, _______, _______, _______, _______,
_______, _______, _______, _______, _______, _______, _______,
_______, _______, _______, _______, _______, _______,
_______, _______, _______, _______, _______, _______, QMKBEST,
_______, _______, _______, _______, _______,														//3}}}

// Left Thumb {{{3

         _______, _______,
                  _______,
_______, _______, _______,																			//3}}}

// Right Hand {{{3

_______     , _______, _______, _______, _______, _______, _______,
_______     , _______, _______, _______, _______, _______, KC_X   ,
              _______, _______, _______, _______, _______, _______,
AUTO_BRACKET, _______, _______, _______, _______, _______, _______,
                       _______, _______, _______, _______, _______,									//3}}}

// Right Thumb {{{3

_______, _______,
_______,
_______, _______, _______																			//3}}}

), //2}}}

//[BLANK] = LAYOUT_ergodox( //:{{{2
//
//    /* keymap-diagram:
//     * ,--------------------------------------------------.    ,--------------------------------------------------.
//     * |        |      |      |      |      |      |      |    |      |      |      |      |      |      |        |
//     * |--------+------+------+------+------+------+------|    |------+------+------+------+------+------+--------|
//     * |        |      |      |      |      |      |      |    |      |      |      |      |      |      |        |
//     * |--------+------+------+------+======+------|      |    |      |------+======+------+------+------+--------|
//     * |        |      |      |      |      |      |------|    |------|      |      |      |      |      |        |
//     * |--------+------+------+------+======+------|      |    |      |------+======+------+------+------+--------|
//     * |        |      |      |      |      |      |      |    |      |      |      |      |      |      |        |
//     * `--------+------+------+------+------+-------------'    `-------------+------+------+------+------+--------'
//     *   |      |      |      |      |      |                                |      |      |      |      |      |
//     *   `----------------------------------'                                `----------------------------------'
//     *                                       ,-------------.  ,-------------.
//     *                                       |      |      |  |      |      |
//     *                                ,------+------+------|  |------+------+------.
//     *                                |      |      |      |  |      |      |      |
//     *                                |      |      |------|  |------|      |      |
//     *                                |      |      |      |  |      |      |      |
//     *                                `--------------------'  `--------------------'
//     */
//
//// Left Hand {{{3
//
//_______, _______, _______, _______, _______, _______, _______,
//_______, _______, _______, _______, _______, _______, _______,
//_______, _______, _______, _______, _______, _______,
//_______, _______, _______, _______, _______, _______, _______,
//_______, _______, _______, _______, _______,														//3}}}
//
//// Left Thumb {{{3
//
//         _______, _______,
//                  _______,
//_______, _______, _______,																			//3}}}
//
//// Right Hand {{{3
//
//_______, _______, _______, _______, _______, _______, _______,
//_______, _______, _______, _______, _______, _______, _______,
//                  _______, _______, _______, _______, _______, _______,
//_______, _______, _______, _______, _______, _______, _______,
//                  _______, _______, _______, _______, _______,										//3}}}
//
//// Right Thumb {{{3
//
//_______, _______,
//_______,
//_______, _______, _______																			//3}}}
//
//), //2}}}

}; //1}}}

// ::::: Key Combos {{{

// https://beta.docs.qmk.fm/features/feature_combo

enum combos {
  QUOTE_ESC,
  ALT_MINUS,
  ALT_EQUAL
};

const uint16_t PROGMEM esc_combo[] = {KC_LEFT_CTRL, KC_QUOTE, COMBO_END};
const uint16_t PROGMEM minus_combo[] = {KC_LEFT_CTRL, KC_P9, COMBO_END};
const uint16_t PROGMEM equal_combo[] = {KC_LEFT_CTRL, KC_P0, COMBO_END};

// Uncomment them here to activate
combo_t key_combos[COMBO_COUNT] = {
//  [QUOTE_ESC] = COMBO(esc_combo, KC_ESC),
//  [ALT_MINUS] = COMBO(minus_combo, KC_MINUS),
//  [ALT_EQUAL] = COMBO(equal_combo, KC_EQUAL)
}; //}}}

// ::::: Tap Dance {{{1

// -> Tap Dance Definitions {{{2

typedef struct {
  bool is_press_action;
  int state;
} tap;

//Define a type for as many tap dance states as you need
enum {
  SINGLE_TAP = 1,
  SINGLE_HOLD = 2,
  DOUBLE_TAP = 3,
  DOUBLE_HOLD = 4,
};

tap_dance_action_t tap_dance_actions[] = {
	//Tap once for Esc, twice for Caps Lock
	[OMNIBRKT_L]  = ACTION_TAP_DANCE_DOUBLE(KC_LEFT_BRACKET, KC_LEFT_PAREN),
	[OMNIBRKT_R]  = ACTION_TAP_DANCE_DOUBLE(KC_RIGHT_BRACKET, KC_RIGHT_PAREN),
	[MNUS_EQ] = ACTION_TAP_DANCE_DOUBLE(KC_MINUS, KC_EQUAL),
//	[ELSFT] = ACTION_TAP_DANCE_LAYER_TOGGLE(KC_LEFT_SHIFT, DOUBLES),
    [DBLSHFT_L] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, doubles_action_left, doubles_reset_left),
    [DBLSHFT_R] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, doubles_action_right, doubles_reset_right),
    [VIM_ESC] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, vim_keys_esc, vim_keys_esc_reset),
	// Other declarations would go here, separated by commas, if you have them
}; // 2}}}

// -> Tap Dance Functions {{{2

//Determine the current tap dance state
int cur_dance (tap_dance_state_t *state) {
  if (state->count == 1) {
    if (!state->pressed) {
      return SINGLE_TAP;
    } else {
      return SINGLE_HOLD;
    }
  } else if (state->count == 2) {
    if (!state->pressed) {
      return DOUBLE_TAP;
    } else {
      return DOUBLE_HOLD;
	}
  }
  else return 8;
}

//Initialize tap structure associated with example tap dance key
static tap ql_tap_state = {
  .is_press_action = true,
  .state = 0
};

// vim_keys_esc {{{3

void vim_keys_esc (tap_dance_state_t *state, void *user_data) {
  ql_tap_state.state = cur_dance(state);
  switch (ql_tap_state.state) {
//    case SINGLE_TAP: 
//      register_code(KC_LEFT_SHIFT); 
//      break;
    case SINGLE_HOLD: 
      layer_on(VIM_KEYS); 
      break;
    case DOUBLE_TAP:
		register_code(KC_ESC);
		break;
//    case DOUBLE_HOLD:
//		layer_on(DOUBLES);
//		register_code(KC_LEFT_SHIFT);
//		break;
  }
}

void vim_keys_esc_reset (tap_dance_state_t *state, void *user_data) {
  //if the key was held down and now is released then switch off the layer
//  if (ql_tap_state.state==DOUBLE_HOLD) {
//    layer_off(DOUBLES);
//    unregister_code(KC_LEFT_SHIFT);
//  }
  if (ql_tap_state.state==DOUBLE_TAP) {
    unregister_code(KC_ESC);
  }
  if (ql_tap_state.state==SINGLE_HOLD) {
    layer_off(VIM_KEYS);
  }
//  if (ql_tap_state.state==SINGLE_TAP) {
//    unregister_code(KC_LEFT_SHIFT);
//  }
  ql_tap_state.state = 0;
} // 3}}}

// doubles_action_left {{{3

void doubles_action_left (tap_dance_state_t *state, void *user_data) {
  ql_tap_state.state = cur_dance(state);
  switch (ql_tap_state.state) {
    case SINGLE_TAP: 
      register_code(KC_LEFT_SHIFT); 
      break;
    case SINGLE_HOLD: 
      register_code(KC_LEFT_SHIFT); 
      break;
    case DOUBLE_TAP:
		layer_on(DOUBLES);
		register_code(KC_LEFT_SHIFT);
		break;
    case DOUBLE_HOLD:
		layer_on(DOUBLES);
		register_code(KC_LEFT_SHIFT);
		break;
  }
}

void doubles_reset_left (tap_dance_state_t *state, void *user_data) {
  //if the key was held down and now is released then switch off the layer
  if (ql_tap_state.state==DOUBLE_HOLD) {
    layer_off(DOUBLES);
    unregister_code(KC_LEFT_SHIFT);
  }
  if (ql_tap_state.state==DOUBLE_TAP) {
    layer_off(DOUBLES);
    unregister_code(KC_LEFT_SHIFT);
  }
  if (ql_tap_state.state==SINGLE_HOLD) {
    unregister_code(KC_LEFT_SHIFT);
  }
  if (ql_tap_state.state==SINGLE_TAP) {
    unregister_code(KC_LEFT_SHIFT);
  }
  ql_tap_state.state = 0;
} // 3}}}

// doubles_action_right {{{3

void doubles_action_right (tap_dance_state_t *state, void *user_data) {
  ql_tap_state.state = cur_dance(state);
  switch (ql_tap_state.state) {
    case SINGLE_TAP: 
      register_code(KC_RIGHT_SHIFT); 
      break;
    case SINGLE_HOLD: 
      register_code(KC_RIGHT_SHIFT); 
      break;
    case DOUBLE_TAP:
		layer_on(DOUBLES);
		register_code(KC_RIGHT_SHIFT);
		break;
    case DOUBLE_HOLD:
		layer_on(DOUBLES);
		register_code(KC_RIGHT_SHIFT);
		break;
  }
}

void doubles_reset_right (tap_dance_state_t *state, void *user_data) {
  //if the key was held down and now is released then switch off the layer
  if (ql_tap_state.state==DOUBLE_HOLD) {
    layer_off(DOUBLES);
    unregister_code(KC_LEFT_SHIFT);
  }
  if (ql_tap_state.state==DOUBLE_TAP) {
    layer_off(DOUBLES);
    unregister_code(KC_LEFT_SHIFT);
  }
  if (ql_tap_state.state==SINGLE_HOLD) {
    unregister_code(KC_RIGHT_SHIFT);
  }
  if (ql_tap_state.state==SINGLE_TAP) {
    unregister_code(KC_RIGHT_SHIFT);
  }
  ql_tap_state.state = 0;
} // 3}}}

//2}}}
//1}}}

bool suspended = false;

bool process_record_user(uint16_t keycode, keyrecord_t *record) {

  switch (keycode) {
    case EPRM:
      if (record->event.pressed) {
        eeconfig_init();
      }
      return false;
      break;
//    case RGB_SLD:
//      if (record->event.pressed) {
//        rgblight_mode(1);
//      }
//      return false;
//      break;
    case QMKBEST:
      if (record->event.pressed) {
        SEND_STRING ("test: QMK rocks!");
      }
      return false;
      break;
    case AUTO_PAREN:
      if (record->event.pressed) {
        SEND_STRING ("()");
        clear_mods();
        SEND_STRING (SS_TAP(X_LEFT));
      }
      return false;
      break;
    case AUTO_BRACKET:
      if (record->event.pressed) {
        SEND_STRING ("[]");
        clear_mods();
        SEND_STRING (SS_TAP(X_LEFT));
      } 
      return false;
      break;
    case AUTO_QUOTE:
      if (record->event.pressed) {
        SEND_STRING ("''");
        clear_mods();
        SEND_STRING (SS_TAP(X_LEFT));
      }
      return false;
      break;
  }
  return true;
}

// Runs just one time when the keyboard initializes
void matrix_init_user(void) {

};

//uint32_t layer_state_set_user(uint32_t state) {

//    uint8_t layer = biton32(state);

//  Runs constantly in the background, in a loop.
void matrix_scan_user(void) {

    uint8_t layer = biton32(layer_state);

    ergodox_board_led_off();
    ergodox_right_led_1_off();
    ergodox_right_led_2_off();
    ergodox_right_led_3_off();
    switch (layer) {
      case 1:
        ergodox_right_led_1_on();
        break;
      case 2:
        ergodox_right_led_2_on();
        break;
      case 3:
        ergodox_right_led_3_on();
        break;
      case 4:
        ergodox_right_led_1_on();
        ergodox_right_led_2_on();
        break;
      case 5:
        ergodox_right_led_1_on();
        ergodox_right_led_3_on();
        break;
      case 6:
        ergodox_right_led_2_on();
        ergodox_right_led_3_on();
        break;
      case 7:
        ergodox_right_led_1_on();
        ergodox_right_led_2_on();
        ergodox_right_led_3_on();
        break;
      default:
        break;
    }
//    return state;

};
