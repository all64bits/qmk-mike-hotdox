/*
  Set any config.h overrides for your specific keymap here.
  See config.h options at https://docs.qmk.fm/#/config_options?id=the-configh-file
*/
#define COMBO_COUNT 3
#define COMBO_TERM 900
#undef TAPPING_TERM
#define TAPPING_TERM 275
