// Instructions for compilation & flashing:
// Run "sudo make hotdox:mwilliams1124:flash" in the QMK root directory
// Then press boot mode button on keyboard
//
// Use this function from Cam's source instead of clear_mods()
// - if using clear_mods() the mods will remain cleared after
// the keypress is complete even if the mod is still held
//
/* #define WITHOUT_MODS(...) \
     do { \
       uint8_t _real_mods = get_mods(); \
       clear_mods(); \
       { __VA_ARGS__ } \
       set_mods(_real_mods); \
     } while (0) 				*/

#include QMK_KEYBOARD_H
#include "version.h"
#include "quantum.h"

// Layer declarations
#define PRIMARY 0 // Default layer
#define LAYER_ONE 1 // WIP
#define LAYER_TWO 2 // WIP
#define LAYER_THREE 3 // WIP
#define LAYER_FOUR 4 // WIP
#define VIM_KEYS 5
#define DOUBLES 6
//#define BLANK 20 // Blank, unused layer.
// Note; if the last layer number is too high, the firmware gets too large

enum custom_keycodes {
  RGB_SLD = SAFE_RANGE, // can always be here
  TOGGLE_LAYER_COLOR,
  EPRM,
  DYNAMIC_MACRO_RANGE,
  QMKBEST,
  AUTO_PAREN,
  AUTO_BRACKET,
  AUTO_QUOTE,
};

// Tap Dance keycodes
enum {
  OMNIBRKT_L = 0,
  OMNIBRKT_R,
  MNUS_EQ,
//  ELSFT,
  DBLSHFT_L,
  DBLSHFT_R,
  VIM_ESC,
};

// Tap Dance declarations

//Function associated with all tap dances
int cur_dance (tap_dance_state_t *state);

//Functions associated with individual tap dances
void doubles_action_left (tap_dance_state_t *state, void *user_data);
void doubles_reset_left (tap_dance_state_t *state, void *user_data);
void doubles_action_right (tap_dance_state_t *state, void *user_data);
void doubles_reset_right (tap_dance_state_t *state, void *user_data);
void vim_keys_esc (tap_dance_state_t *state, void *user_data);
void vim_keys_esc_reset (tap_dance_state_t *state, void *user_data);

// ::::: KEYMAPS {{{1

// Considerations/thoughts:
// Shift, Ctrl & Alt output their normal value and at same time switch to function layer.
// Need another layer key for function layer without chording
// LM(layer, mod) - Momentarily activates layer (like MO), but with modifier(s) mod active. Only supports layers 0-15 and the left modifiers: MOD_LCTL, MOD_LSFT, MOD_LALT, MOD_LGUI (note the use of MOD_ constants instead of KC_). These modifiers can be combined using bitwise OR, e.g. LM(FUNC, MOD_LCTL | MOD_LALT).
//
// Pad layer - shebangs, boilerplate etc
// Use Tap Dance for RESET (bootmagic keycode)?

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

[PRIMARY] = LAYOUT_ergodox( //:{{{2

    /* keymap-diagram (old):
     * ,--------------------------------------------------.    ,--------------------------------------------------.
     * |    ~   |   1  |   2  |   3  |   4  |   5  | '""' |    | '""' |   6  |   7  |   8  |   9  |   0  |  -/=   |
     * |--------+------+------+------+------+------+------|    |------+------+------+------+------+------+--------|
     * |  Tab   |   Q  |   W  |   E  |   R  |   T  |  ()  |    |  []  |   Y  |   U  |   I  |   O  |   P  |   \    |
     * |--------+------+------+------+======+------|      |    |      |------+======+------+------+------+--------|
     * |  Ctrl  |   A  |   S  |   D  |   F  |   G  |------|    |------|   H  |   J  |   K  |   L  |   ;  |   '    |
     * |--------+------+------+------+======+------|  [{( |    | )}]  |------+======+------+------+------+--------|
     * | DbShft |   Z  |   X  |   C  |   V  |   B  |      |    |      |   N  |   M  |   ,  |   .  |   /  | DbShft |
     * `--------+------+------+------+------+-------------'    `-------------+------+------+------+------+--------'
     *   | PgUp | PgDn | Home | End  |VimEsc|                                | Del  | Ins  | Meh  | SysR | Hypr |
     *   `----------------------------------'                                `----------------------------------'
     *                                       ,-------------.  ,-------------.
     *                                       | Meta |  Fx  |  |  Fx  | Meta |
     *                                ,------+------+------|  |------+------+------.
     *                                |      |      | NmPd |  |      |      |      |
     *                                | Entr | Bksp |------|  |------| Ctrl | Spc  |
     *                                |      |      | Alt  |  | Alt  |      |      |
     *                                `--------------------'  `--------------------'
     */
    /* keymap-diagram (new):
     *
     *
     *          ,----------------------------------.                  ,----------------------------------.
     *          |   Q  |   W  |   E  |   R  |   T  |                  |   Y  |   U  |   I  |   O  |   P  |
     *          |------+------+------+======+------|                  |------+======+------+------+------|
     *          |   A  |   S  |   D  |   F  |   G  |                  |   H  |   J  |   K  |   L  |   ;  |
     *          |------+------+------+======+------|                  |------+======+------+------+------|
     *          | LT Z |   X  |   C  |   V  |   B  |                  |   N  |   M  |   ,  |   .  | LT / |
     *          `------+------+------+------+------'                  `----------------------------------'
     *                               | TT(3)|                                |LMCtrl|
     *                               `------'                                `------'
     *
     *
     *                                ,-------------.                ,-------------.
     *                                | LM   | LM   |                | LM   | LT   |
     *                                | Ctrl | Alt  |                | Alt  | Spc  |
     *                                |      |      |                |      |      |
     *                                `-------------'                `-------------'
     *
     *  LT = Activates layer when held, sends keycode when tapped
     *  LM = Activates layer, with modifier also active
     */
//Caps KC_CAPSLOCK - make this available somewhere. Shift double tap??
//Remove one of the Fx layer keys once you have auto-switch-to-fx-layer-with-modifier working

// Left Hand

_______ , _______ , _______ , _______ , _______     , _______ , _______ ,
_______ , KC_Q    , KC_W    , KC_E    , KC_R        , KC_T    , _______ ,
_______ , KC_A    , KC_S    , KC_D    , LT(4, KC_F) , KC_G    ,
_______ , KC_Z    , KC_X    , KC_C    , KC_V        , KC_B    , _______ ,
_______ , _______ , _______ , _______ , MO(1)       ,

// Left Thumb

              _______ , _______ ,
                        _______ ,
OSM(MOD_LSFT) , MO(3) , _______ ,

// Right Hand

_______ , _______       , _______     , _______  , _______ , _______      , _______ ,
_______ , KC_Y          , KC_U        , KC_I     , KC_O    , KC_P         , _______ ,
          KC_H          , LT(4, KC_J) , KC_K     , KC_L    , KC_SEMICOLON , _______ ,
_______ , KC_N          , KC_M        , KC_COMMA , KC_DOT  , KC_SLASH     , _______ ,
          OSM(MOD_RCTL) , _______     , _______  , _______ , _______      ,

// Right Thumb

_______ , _______       ,
_______ ,
_______ , OSM(MOD_RALT) , LT(2, KC_SPACE)

), // 2}}}

/***************************
 LAYER ONE (SECONDARY LAYER)
 ***************************/
[LAYER_ONE] = LAYOUT_ergodox( //:{{{2

// Left Hand
_______ , _______ , _______ , _______ , _______ , _______         , _______ ,
_______ , _______ , _______ , _______ , _______ , KC_PRINT_SCREEN , _______ ,
_______ , _______ , _______ , _______ , _______ , _______         ,
_______ , _______ , _______ , _______ , _______ , _______         , _______ ,
_______ , _______ , _______ , _______ , _______ ,

// Left Thumb
          _______ , _______ ,
                    _______ ,
_______ , _______ , _______ ,


// Right Hand
_______ , _______      , _______   , _______ , _______   , _______      , _______ ,
_______ , KC_MEH       , KC_HYPR   , _______ , KC_ESCAPE , _______      , _______ ,
          KC_BACKSPACE , KC_INSERT , _______ , _______   , _______      , _______ ,
_______ , KC_DELETE    , _______   , _______ , _______   , KC_RIGHT_GUI , _______ ,
                         _______   , _______ , _______   , _______      , _______ ,

// Right Thumb
_______ , _______ ,
_______ ,
_______ , _______ , _______

), //2}}}

/***************************
 LAYER TWO (TERTIARY LAYER)
 ***************************/
[LAYER_TWO] = LAYOUT_ergodox( //:{{{2

// Left Hand
_______ , _______       , _______      , _______         , _______          , _______     , _______ ,
_______ , KC_TAB        , KC_BACKSLASH , KC_LEFT_PAREN   , KC_RIGHT_PAREN   , KC_GRAVE    , _______ ,
_______ , KC_EXCLAIM    , KC_AT        , KC_HASH         , KC_DOLLAR        , KC_PERCENT  ,
_______ , KC_CIRCUMFLEX , KC_AMPERSAND , KC_LEFT_BRACKET , KC_RIGHT_BRACKET , KC_ASTERISK , _______ ,
_______ , _______       , _______      , _______         , _______          ,

// Left Thumb
          _______ , _______ ,
                    _______ ,
_______ , _______ , _______ ,


// Right Hand
_______ , _______ , _______ , _______ , _______ , _______  , _______ ,
_______ , _______ , _______ , _______ , _______ , _______  , _______ ,
          _______ , _______ , _______ , _______ , KC_QUOTE , _______ ,
_______ , _______ , _______ , _______ , _______ , _______  , _______ ,
                    _______ , _______ , _______ , _______  , _______ ,

// Right Thumb
_______ , _______ ,
_______ ,
_______ , _______ , _______

), //2}}}

/***************************
 LAYER THREE (QUATERNARY LAYER)
 ***************************/
[LAYER_THREE] = LAYOUT_ergodox( //:{{{2

// Left Hand
_______ , _______ , _______ , _______ , _______  , _______ , _______ ,
_______ , _______ , _______ , KC_UP   , _______  , _______ , _______ ,
_______ , _______ , KC_LEFT , KC_DOWN , KC_RIGHT , _______ ,
_______ , _______ , _______ , _______ , _______  , _______ , _______ ,
_______ , _______ , _______ , _______ , _______  ,

// Left Thumb
          _______ , _______ ,
                    _______ ,
_______ , _______ , _______ ,


// Right Hand
_______ , _______        , _______   , _______ , _______ , _______  , _______ ,
_______ , KC_KP_SLASH    , KC_KP_7   , KC_KP_8 , KC_KP_9 , KC_MINUS , _______ ,
          KC_KP_ASTERISK , KC_KP_4   , KC_KP_5 , KC_KP_6 , KC_EQUAL , _______ ,
_______ , KC_KP_0        , KC_KP_1   , KC_KP_2 , KC_KP_3 , KC_ENTER , _______ ,
                           KC_KP_DOT , _______ , _______ , _______  , _______ ,

// Right Thumb
_______ , _______ ,
_______ ,
_______ , _______ , _______

), //2}}}

/***************************
 LAYER FOUR (QUINARY LAYER)
 ***************************/
[LAYER_FOUR] = LAYOUT_ergodox( //:{{{2

// Left Hand
_______ , _______ , _______ , _______ , _______ , _______ , _______ ,
_______ , KC_F1   , KC_F2   , KC_F3   , KC_F4   , KC_F5   , _______ ,
_______ , _______ , _______ , _______ , _______ , _______ ,
_______ , KC_F11  , KC_F12  , KC_F13  , KC_F14  , KC_F15  , _______ ,
_______ , _______ , _______ , _______ , _______ ,

// Left Thumb
          _______ , _______ ,
                    _______ ,
KC_CAPS_LOCK , KC_NUM_LOCK  , _______ ,


// Right Hand
_______ , _______ , _______ , _______ , _______ , _______ , _______ ,
_______ , KC_F6   , KC_F7   , KC_F8   , KC_F9   , KC_F10  , _______ ,
          _______ , _______ , _______ , _______ , _______ , _______ ,
_______ , KC_F16  , KC_F17  , KC_F18  , KC_F19  , _______ , _______ ,
                    _______ , _______ , _______ , _______ , _______ ,

// Right Thumb
_______ , _______    ,
_______ ,
_______ , KC_PAGE_UP , KC_PAGE_DOWN

), //2}}}

//[BLANK] = LAYOUT_ergodox(
//
//    /* keymap-diagram:
//     * ,--------------------------------------------------.    ,--------------------------------------------------.
//     * |        |      |      |      |      |      |      |    |      |      |      |      |      |      |        |
//     * |--------+------+------+------+------+------+------|    |------+------+------+------+------+------+--------|
//     * |        |      |      |      |      |      |      |    |      |      |      |      |      |      |        |
//     * |--------+------+------+------+======+------|      |    |      |------+======+------+------+------+--------|
//     * |        |      |      |      |      |      |------|    |------|      |      |      |      |      |        |
//     * |--------+------+------+------+======+------|      |    |      |------+======+------+------+------+--------|
//     * |        |      |      |      |      |      |      |    |      |      |      |      |      |      |        |
//     * `--------+------+------+------+------+-------------'    `-------------+------+------+------+------+--------'
//     *   |      |      |      |      |      |                                |      |      |      |      |      |
//     *   `----------------------------------'                                `----------------------------------'
//     *                                       ,-------------.  ,-------------.
//     *                                       |      |      |  |      |      |
//     *                                ,------+------+------|  |------+------+------.
//     *                                |      |      |      |  |      |      |      |
//     *                                |      |      |------|  |------|      |      |
//     *                                |      |      |      |  |      |      |      |
//     *                                `--------------------'  `--------------------'
//     */
//
//// Left Hand
//_______ , _______ , _______ , _______ , _______ , _______ , _______ ,
//_______ , _______ , _______ , _______ , _______ , _______ , _______ ,
//_______ , _______ , _______ , _______ , _______ , _______ ,
//_______ , _______ , _______ , _______ , _______ , _______ , _______ ,
//_______ , _______ , _______ , _______ , _______ ,
//
//// Left Thumb
//          _______ , _______ ,
//                    _______ ,
//_______ , _______ , _______ ,
//
//
//// Right Hand
//_______ , _______ , _______ , _______ , _______ , _______ , _______ ,
//_______ , _______ , _______ , _______ , _______ , _______ , _______ ,
//          _______ , _______ , _______ , _______ , _______ , _______ ,
//_______ , _______ , _______ , _______ , _______ , _______ , _______ ,
//                    _______ , _______ , _______ , _______ , _______ ,
//
//// Right Thumb
//_______ , _______ ,
//_______ ,
//_______ , _______ , _______

//),

}; //1}}}

// ::::: Key Combos {{{

// https://beta.docs.qmk.fm/features/feature_combo

enum combos {
  QUOTE_ESC,
  ALT_MINUS,
  ALT_EQUAL
};

const uint16_t PROGMEM esc_combo[] = {KC_LEFT_CTRL, KC_QUOTE, COMBO_END};
const uint16_t PROGMEM minus_combo[] = {KC_LEFT_CTRL, KC_P9, COMBO_END};
const uint16_t PROGMEM equal_combo[] = {KC_LEFT_CTRL, KC_P0, COMBO_END};

// Uncomment them here to activate
combo_t key_combos[COMBO_COUNT] = {
//  [QUOTE_ESC] = COMBO(esc_combo, KC_ESC),
//  [ALT_MINUS] = COMBO(minus_combo, KC_MINUS),
//  [ALT_EQUAL] = COMBO(equal_combo, KC_EQUAL)
}; //}}}

// ::::: Tap Dance {{{1

// -> Tap Dance Definitions {{{2

typedef struct {
  bool is_press_action;
  int state;
} tap;

//Define a type for as many tap dance states as you need
enum {
  SINGLE_TAP = 1,
  SINGLE_HOLD = 2,
  DOUBLE_TAP = 3,
  DOUBLE_HOLD = 4,
};

tap_dance_action_t tap_dance_actions[] = {
	//Tap once for Esc, twice for Caps Lock
	[OMNIBRKT_L]  = ACTION_TAP_DANCE_DOUBLE(KC_LEFT_BRACKET, KC_LEFT_PAREN),
	[OMNIBRKT_R]  = ACTION_TAP_DANCE_DOUBLE(KC_RIGHT_BRACKET, KC_RIGHT_PAREN),
	[MNUS_EQ] = ACTION_TAP_DANCE_DOUBLE(KC_MINUS, KC_EQUAL),
//	[ELSFT] = ACTION_TAP_DANCE_LAYER_TOGGLE(KC_LEFT_SHIFT, DOUBLES),
    [DBLSHFT_L] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, doubles_action_left, doubles_reset_left),
    [DBLSHFT_R] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, doubles_action_right, doubles_reset_right),
    [VIM_ESC] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, vim_keys_esc, vim_keys_esc_reset),
	// Other declarations would go here, separated by commas, if you have them
}; // 2}}}

// -> Tap Dance Functions {{{2

//Determine the current tap dance state
int cur_dance (tap_dance_state_t *state) {
  if (state->count == 1) {
    if (!state->pressed) {
      return SINGLE_TAP;
    } else {
      return SINGLE_HOLD;
    }
  } else if (state->count == 2) {
    if (!state->pressed) {
      return DOUBLE_TAP;
    } else {
      return DOUBLE_HOLD;
	}
  }
  else return 8;
}

//Initialize tap structure associated with example tap dance key
static tap ql_tap_state = {
  .is_press_action = true,
  .state = 0
};

// vim_keys_esc {{{3

void vim_keys_esc (tap_dance_state_t *state, void *user_data) {
  ql_tap_state.state = cur_dance(state);
  switch (ql_tap_state.state) {
//    case SINGLE_TAP: 
//      register_code(KC_LEFT_SHIFT); 
//      break;
    case SINGLE_HOLD: 
      layer_on(VIM_KEYS); 
      break;
    case DOUBLE_TAP:
		register_code(KC_ESC);
		break;
//    case DOUBLE_HOLD:
//		layer_on(DOUBLES);
//		register_code(KC_LEFT_SHIFT);
//		break;
  }
}

void vim_keys_esc_reset (tap_dance_state_t *state, void *user_data) {
  //if the key was held down and now is released then switch off the layer
//  if (ql_tap_state.state==DOUBLE_HOLD) {
//    layer_off(DOUBLES);
//    unregister_code(KC_LEFT_SHIFT);
//  }
  if (ql_tap_state.state==DOUBLE_TAP) {
    unregister_code(KC_ESC);
  }
  if (ql_tap_state.state==SINGLE_HOLD) {
    layer_off(VIM_KEYS);
  }
//  if (ql_tap_state.state==SINGLE_TAP) {
//    unregister_code(KC_LEFT_SHIFT);
//  }
  ql_tap_state.state = 0;
} // 3}}}

// doubles_action_left {{{3

void doubles_action_left (tap_dance_state_t *state, void *user_data) {
  ql_tap_state.state = cur_dance(state);
  switch (ql_tap_state.state) {
    case SINGLE_TAP: 
      register_code(KC_LEFT_SHIFT); 
      break;
    case SINGLE_HOLD: 
      register_code(KC_LEFT_SHIFT); 
      break;
    case DOUBLE_TAP:
		layer_on(DOUBLES);
		register_code(KC_LEFT_SHIFT);
		break;
    case DOUBLE_HOLD:
		layer_on(DOUBLES);
		register_code(KC_LEFT_SHIFT);
		break;
  }
}

void doubles_reset_left (tap_dance_state_t *state, void *user_data) {
  //if the key was held down and now is released then switch off the layer
  if (ql_tap_state.state==DOUBLE_HOLD) {
    layer_off(DOUBLES);
    unregister_code(KC_LEFT_SHIFT);
  }
  if (ql_tap_state.state==DOUBLE_TAP) {
    layer_off(DOUBLES);
    unregister_code(KC_LEFT_SHIFT);
  }
  if (ql_tap_state.state==SINGLE_HOLD) {
    unregister_code(KC_LEFT_SHIFT);
  }
  if (ql_tap_state.state==SINGLE_TAP) {
    unregister_code(KC_LEFT_SHIFT);
  }
  ql_tap_state.state = 0;
} // 3}}}

// doubles_action_right {{{3

void doubles_action_right (tap_dance_state_t *state, void *user_data) {
  ql_tap_state.state = cur_dance(state);
  switch (ql_tap_state.state) {
    case SINGLE_TAP: 
      register_code(KC_RIGHT_SHIFT); 
      break;
    case SINGLE_HOLD: 
      register_code(KC_RIGHT_SHIFT); 
      break;
    case DOUBLE_TAP:
		layer_on(DOUBLES);
		register_code(KC_RIGHT_SHIFT);
		break;
    case DOUBLE_HOLD:
		layer_on(DOUBLES);
		register_code(KC_RIGHT_SHIFT);
		break;
  }
}

void doubles_reset_right (tap_dance_state_t *state, void *user_data) {
  //if the key was held down and now is released then switch off the layer
  if (ql_tap_state.state==DOUBLE_HOLD) {
    layer_off(DOUBLES);
    unregister_code(KC_LEFT_SHIFT);
  }
  if (ql_tap_state.state==DOUBLE_TAP) {
    layer_off(DOUBLES);
    unregister_code(KC_LEFT_SHIFT);
  }
  if (ql_tap_state.state==SINGLE_HOLD) {
    unregister_code(KC_RIGHT_SHIFT);
  }
  if (ql_tap_state.state==SINGLE_TAP) {
    unregister_code(KC_RIGHT_SHIFT);
  }
  ql_tap_state.state = 0;
} // 3}}}

//2}}}
//1}}}

bool suspended = false;

bool process_record_user(uint16_t keycode, keyrecord_t *record) {

  switch (keycode) {
    case EPRM:
      if (record->event.pressed) {
        eeconfig_init();
      }
      return false;
      break;
//    case RGB_SLD:
//      if (record->event.pressed) {
//        rgblight_mode(1);
//      }
//      return false;
//      break;
    case QMKBEST:
      if (record->event.pressed) {
        SEND_STRING ("test: QMK rocks!");
      }
      return false;
      break;
    case AUTO_PAREN:
      if (record->event.pressed) {
        SEND_STRING ("()");
        clear_mods();
        SEND_STRING (SS_TAP(X_LEFT));
      }
      return false;
      break;
    case AUTO_BRACKET:
      if (record->event.pressed) {
        SEND_STRING ("[]");
        clear_mods();
        SEND_STRING (SS_TAP(X_LEFT));
      } 
      return false;
      break;
    case AUTO_QUOTE:
      if (record->event.pressed) {
        SEND_STRING ("''");
        clear_mods();
        SEND_STRING (SS_TAP(X_LEFT));
      }
      return false;
      break;
  }
  return true;
}

// Runs just one time when the keyboard initializes
void matrix_init_user(void) {

};

//uint32_t layer_state_set_user(uint32_t state) {

//    uint8_t layer = biton32(state);

//  Runs constantly in the background, in a loop.
void matrix_scan_user(void) {

    uint8_t layer = biton32(layer_state);

    ergodox_board_led_off();
    ergodox_right_led_1_off();
    ergodox_right_led_2_off();
    ergodox_right_led_3_off();
    switch (layer) {
      case 1:
        ergodox_right_led_1_on();
        break;
      case 2:
        ergodox_right_led_2_on();
        break;
      case 3:
        ergodox_right_led_3_on();
        break;
      case 4:
        ergodox_right_led_1_on();
        ergodox_right_led_2_on();
        break;
      case 5:
        ergodox_right_led_1_on();
        ergodox_right_led_3_on();
        break;
      case 6:
        ergodox_right_led_2_on();
        ergodox_right_led_3_on();
        break;
      case 7:
        ergodox_right_led_1_on();
        ergodox_right_led_2_on();
        ergodox_right_led_3_on();
        break;
      default:
        break;
    }
//    return state;

};
